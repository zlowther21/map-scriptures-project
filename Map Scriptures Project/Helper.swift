//
//  Helper.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/22/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import Foundation
class Helper {
    class func removeAmpersandAndDashFromString(input: String) -> String {
        return input.stringByReplacingOccurrencesOfString("&amp;", withString: "&").stringByReplacingOccurrencesOfString("&mdash;", withString: "-")
    }
}
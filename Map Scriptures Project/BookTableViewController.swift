//
//  BookTableViewController.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/17/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

class BookTableViewController: UITableViewController {
    var standardWork: Book?
    var books : [Book]!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let chapters = books[indexPath.row].numChapters {
            performSegueWithIdentifier("ChapterSegue", sender: self)
        }else {
            performSegueWithIdentifier("NoChapterWebViewSegue", sender: self)
        }
        
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("BookCell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel?.text = books[indexPath.row].fullName
        return cell
    }
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ChapterSegue" {
            if let destination = segue.destinationViewController as? ChapterTableViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow() {
                    let book = books[indexPath.row] as Book
                    if let numChapters = book.numChapters {
                        destination.book = book
                    }else {
                
                    }
                    destination.title = Helper.removeAmpersandAndDashFromString(book.citeAbbr)
                }
            }
        }else if segue.identifier == "NoChapterWebViewSegue" {
            if let destination = segue.destinationViewController as? ScriptureViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow() {
                    let book = books[indexPath.row] as Book
                    destination.chapter = 0
                    destination.book = book
                    destination.title = Helper.removeAmpersandAndDashFromString(book.citeAbbr)
                    
                }
            }

        }
    }
    
}

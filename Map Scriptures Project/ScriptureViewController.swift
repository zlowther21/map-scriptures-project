//
//  WebViewController.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/18/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

class ScriptureViewController: UIViewController, UIWebViewDelegate, SuggestionDisplayDelegate {
    var chapter: Int!
    var book: Book!
    var selectedGeoplace : GeoPlace?
    @IBOutlet weak var webView: ScriptureWebView!
    weak var mapViewController:MapViewController?
    lazy var backgroundQueue = dispatch_queue_create("background thread", nil)
    var placename, offset : String!

    
    override func viewDidLoad() {
        webView.loadHTMLString(ScriptureRenderer.sharedRenderer.htmlForBookId(book.id, chapter: chapter), baseURL: nil)
        configureMapViewController()
        webView.suggestionDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        configureMapViewController()
    }
    func configureMapViewController() {
        mapViewController = nil
        if let splitVC = splitViewController {
            if let navVC = splitVC.viewControllers.last as? UINavigationController {
                mapViewController = navVC.topViewController as? MapViewController
            }
        }

    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "MapSegue" {
            if let navVC = segue.destinationViewController as? UINavigationController {
                if let mapVC = navVC.topViewController as? MapViewController {
                    
                    mapVC.geoplaces = ScriptureRenderer.sharedRenderer.collectedGeocodedPlaces
                    mapVC.selectedGeoplace = selectedGeoplace
                    changePinIdForColor(mapVC)
                    mapVC.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem()
                    mapVC.navigationItem.leftItemsSupplementBackButton = true
                }
            }
            
        }else if segue.identifier == "AddGeocodeSegue" {
            if let navVC = segue.destinationViewController as? UINavigationController {
                if let modalVC = navVC.topViewController as? FormTableViewController {
                    if let mapVC = mapViewController {
                        let camera = mapVC.mapView.camera
                        
                        modalVC.latText = "\(camera.centerCoordinate.latitude)"
                        modalVC.longText = "\(camera.centerCoordinate.longitude)"
                        modalVC.viewLatText = "\(camera.centerCoordinate.latitude)"
                        modalVC.viewLongText = "\(camera.centerCoordinate.longitude)"
                        modalVC.viewHeadingText = "\(camera.heading)"
                        modalVC.viewTiltText = "\(0.0)"
                        modalVC.viewAltText = "\(camera.altitude)"
                        modalVC.viewRollText = "\(0.0)"
                    }
                }
            }
        }
    }
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.URL.absoluteString?.rangeOfString(ScriptureRenderer.sharedRenderer.BASE_URL) != nil {
            let locationId = request.URL.pathComponents.last!.integerValue
            if let mapVC = mapViewController {
                changePinIdForColor(mapVC)
                if let geoplace = ScriptureRenderer.sharedRenderer.geocodedLatandLongForId(locationId) {
                    mapVC.zoomToLocation(geoplace.latitude,
                        withlongitude: geoplace.longitude,
                        withViewLat: geoplace.viewLatitude,
                        withViewLong: geoplace.viewLongitude,
                        withAltitude: geoplace.viewAltitude,
                        withViewHeading: geoplace.viewHeading)
                    mapVC.navigationController?.navigationBar.topItem?.title = geoplace.placename
                }
            }
            else {
                selectedGeoplace = ScriptureRenderer.sharedRenderer.geocodedLatandLongForId(locationId)
                performSegueWithIdentifier("MapSegue", sender: self)
                
            }
            return false
        }else {
            if let mapVC = mapViewController {
                mapVC.geoplaces = ScriptureRenderer.sharedRenderer.collectedGeocodedPlaces
                mapVC.navigationController?.navigationBar.topItem?.title = mapVC.changeNavbarTitleForMultiplePlaces()
                changePinIdForColor(mapVC)
                mapVC.updateMapAnnotations()
            }
            
        }
        
        
        return true
    }
    
    func changePinIdForColor(mapVC : MapViewController) {
        if let id = book.parentBookId {
            mapVC.bookIdForPinColor = id
        }
    }
    
    //MARK: Suggestion Delegate
    func displaySuggestDialog() {
        performSegueWithIdentifier("AddGeocodeSegue", sender: self)
        
        //placename obtained by following pattern on http://stackoverflow.com/questions/1968872/iphone-development-grabbing-selected-highlighted-text-on-uiwebview
        placename = self.webView.stringByEvaluatingJavaScriptFromString("window.getSelection().toString()")!
        offset = self.webView.stringByEvaluatingJavaScriptFromString("getSelectionOffset()")!
        
    }
    
    
    
    //MARK: Modal Actions
    @IBAction func cancelSuggestion(segue: UIStoryboardSegue){
        
        webView.userInteractionEnabled = false
        webView.userInteractionEnabled = true
        
        dispatch_async(dispatch_get_main_queue()){
            self.dismissViewControllerAnimated(true, completion: nil)
        }
        
    }
    
    @IBAction func saveSuggestion(segue: UIStoryboardSegue){
        if let modalVC = segue.sourceViewController as? FormTableViewController {
            
            // If string does not have valid number, the value is set to 0.0 automatically. Nice conversion without crashing
            let tempLatitude = (modalVC.latitude.text as NSString).doubleValue
            let tempLongitude = (modalVC.longitude.text as NSString).doubleValue

            //Vars other than lat and long, if not set, should be set to zero
            var tempViewLat, tempViewLong, tempViewTilt, tempViewRoll, tempViewAltitude, tempViewHeading : Double!
            tempViewLat = modalVC.viewLatitude.text.isEmpty ? 0.0 : (modalVC.viewLatitude.text as NSString).doubleValue
            tempViewLong = modalVC.viewLongitude.text.isEmpty ? 0.0 : (modalVC.viewLongitude.text as NSString).doubleValue
            tempViewTilt = modalVC.viewTilt.text.isEmpty ? 0.0 : (modalVC.viewTilt.text as NSString).doubleValue
            tempViewRoll = modalVC.viewRoll.text.isEmpty ? 0.0 : (modalVC.viewRoll.text as NSString).doubleValue
            tempViewAltitude = modalVC.viewAltitude.text.isEmpty ? 0.0 : (modalVC.viewAltitude.text as NSString).doubleValue
            tempViewHeading = modalVC.viewHeading.text.isEmpty ? 0.0 : (modalVC.viewHeading.text as NSString).doubleValue
            
          
            
            dispatch_async(backgroundQueue) {
                var sessionConfig = NSURLSessionConfiguration.ephemeralSessionConfiguration()
                sessionConfig.allowsCellularAccess = true
                sessionConfig.timeoutIntervalForRequest = 15.0
                sessionConfig.timeoutIntervalForResource = 15.0
                sessionConfig.HTTPMaximumConnectionsPerHost = 2
                
                var session = NSURLSession(configuration: sessionConfig)
                
                //Need to URL Encode for placenames that have spaces
                var offsetEncoded = self.offset.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
                var placenameEncoded = self.placename.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
                
                
                
                let urlString = "http://scriptures.byu.edu/mapscrip/suggestpm.php?placename=\(placenameEncoded)&latitude=\(tempLatitude)&longitude=\(tempLongitude)&viewLatitude=\(tempViewLat)&viewLongitude=\(tempViewLong)&viewTilt=\(tempViewTilt)&viewRoll=\(tempViewRoll)&viewAltitude=\(tempViewAltitude)&viewHeading=\(tempViewHeading)&offset=\(offsetEncoded)&bookId=\(self.book.id)&chapter=\(self.chapter)"
                
                var urlStringEncoded = urlString.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
                
                var url = NSURL(string: urlStringEncoded)
                println(urlStringEncoded)
                var request = NSURLRequest(URL: url!)
                var task = session.dataTaskWithRequest(request) {
                    (data: NSData!, response: NSURLResponse!, error: NSError!) in
                    var succeeded = false
                    if error == nil {
                        var jsonError: NSError?
                        let resultRecord : AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options: .allZeros, error: &jsonError)
                
                        if jsonError == nil && resultRecord is NSDictionary {
                            let resultDict = resultRecord as NSDictionary
                            let resultCode = resultDict["result"] as Int
                            if resultCode == 0 {
                                succeeded = true
                            }
                        }
                    }
                    if !succeeded {
                        var alertController = UIAlertController(title: "Error", message: "An error occurred while trying to save your suggestion. Please check your network connection and try again.", preferredStyle: .Alert)
                        var okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
                            alertController.addAction(okAction)
                            dispatch_async(dispatch_get_main_queue()){
                                self.presentViewController(alertController, animated: true, completion: nil)
                            }
                    }
                
                
                }
            
                
                task.resume()
                    
                dispatch_async(dispatch_get_main_queue()){
                    self.webView.userInteractionEnabled = false
                    self.webView.userInteractionEnabled = true
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            
            }
        }
    }
    
}

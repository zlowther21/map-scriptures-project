//
//  AppDelegate.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/10/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UISplitViewControllerDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        let splitViewController = self.window!.rootViewController as UISplitViewController
        let navigationController = splitViewController.viewControllers[splitViewController.viewControllers.count-1] as UINavigationController
        navigationController.topViewController.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
        splitViewController.delegate = self
        
        UIMenuController.sharedMenuController().menuItems = [UIMenuItem(title: "Suggest Geocode", action: "suggestGeocoding:")!]
        return true
    }

       // MARK: - Split view

    func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController:UIViewController!, ontoPrimaryViewController primaryViewController:UIViewController!) -> Bool {
        if let secondaryAsNavController = secondaryViewController as? UINavigationController {
            if let topAsDetailController = secondaryAsNavController.topViewController as? MapViewController {
                if topAsDetailController.geoplaces.count == 0 {
                    // Return true to indicate that we have handled the collapse by doing nothing; the secondary controller will be discarded.
                    return true
                }
                topAsDetailController.navigationItem.leftItemsSupplementBackButton = true
              
            }
        }
        return false
    }
    
    func splitViewController(splitViewController: UISplitViewController, separateSecondaryViewControllerFromPrimaryViewController primaryViewController: UIViewController!) -> UIViewController? {
        if let navVC = primaryViewController as? UINavigationController {
            for controller in navVC.viewControllers {
                if let controllerVC = controller as? UINavigationController {
                    return controllerVC
                }
            }
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let detailVC = storyboard.instantiateViewControllerWithIdentifier("detailVC") as UINavigationController
       
        let controller = detailVC.visibleViewController
        controller.navigationItem.leftBarButtonItem = splitViewController.displayModeButtonItem()
        controller.navigationItem.leftItemsSupplementBackButton = true
        return detailVC
    }

}


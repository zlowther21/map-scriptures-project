//
//  ScriptureWebView.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 12/3/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

protocol SuggestionDisplayDelegate {
    func displaySuggestDialog()
}
class ScriptureWebView : UIWebView{
    var suggestionDelegate : SuggestionDisplayDelegate!
    func suggestGeocoding(sender: AnyObject?) {
        suggestionDelegate.displaySuggestDialog()
    }
    override func canPerformAction(action: Selector, withSender sender: AnyObject?) -> Bool {
        if action == "suggestGeocoding:" {
            return true
        }
        return super.canPerformAction(action, withSender: sender)
    }
    
}

//
//  ChapterTableViewController.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/17/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

class ChapterTableViewController: UITableViewController {
    var book : Book!
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return book.numChapters!
    }
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCellWithIdentifier("ChapterCell", forIndexPath: indexPath) as UITableViewCell
        if let parentId = book.parentBookId{
            if parentId == 4 {
                cell.textLabel?.text = "Section \(indexPath.row + 1)"
            }
            else {
                cell.textLabel?.text = "Chapter \(indexPath.row + 1)"
                
            }
        }
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "WebViewSegue" {
            if let destination = segue.destinationViewController as? ScriptureViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow() {
                    let chapter = indexPath.row + 1
                    destination.book = book
                    destination.chapter = chapter
                    destination.title = "\(Helper.removeAmpersandAndDashFromString(book.citeAbbr)) \(chapter)"
                }
            }
        }
    }
}

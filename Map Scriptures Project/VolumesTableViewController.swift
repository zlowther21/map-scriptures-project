//
//  MasterViewController.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/10/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

class VolumesTableViewController: UITableViewController {

    var mapViewController: MapViewController? = nil
    lazy var objects = GeoDatabase.sharedGeoDatabase.volumes()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let controllers = self.splitViewController!.viewControllers
        self.mapViewController = controllers.last?.topViewController as? MapViewController
    }



    // MARK: - Segues

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "BookSegue" {
            if let destination = segue.destinationViewController as? BookTableViewController {
                if let indexPath = self.tableView.indexPathForSelectedRow() {
                    let book = objects[indexPath.row] as Book
                    destination.books = GeoDatabase.sharedGeoDatabase.booksForParentId(book.id)
                    destination.title = Helper.removeAmpersandAndDashFromString(book.citeAbbr)
                }
            }
        }
    }

    // MARK: - Table View

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("StandardWorkCell", forIndexPath: indexPath) as UITableViewCell

        let object = objects[indexPath.row] as Book
        cell.textLabel?.text = object.fullName as String
        return cell
    }



}


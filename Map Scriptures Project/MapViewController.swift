//
//  MapViewController.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 11/10/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    //MARK: Variables and Outlets
    let locationManager = CLLocationManager()
    var bookIdForPinColor: Int?
    lazy var geoplaces = [GeoPlace]()
    var selectedGeoplace : GeoPlace?
    @IBOutlet weak var mapView: MKMapView!

    //MARK: View Initialization
    func configureView() {
        if let splitVC = splitViewController {
            if splitVC.collapsed == false && ScriptureRenderer.sharedRenderer.collectedGeocodedPlaces.count == 0{
                // Ask for Authorization from the User.
                self.locationManager.requestAlwaysAuthorization()
                
                // For use in foreground
                self.locationManager.requestWhenInUseAuthorization()
                
                if CLLocationManager.locationServicesEnabled() {
                    locationManager.delegate = self
                    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                    locationManager.startUpdatingLocation()
                }
            }
            
        }
        
        
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
       
    }
    
    override func viewDidAppear(animated: Bool) {
        if geoplaces.count == 0 {
            geoplaces = ScriptureRenderer.sharedRenderer.collectedGeocodedPlaces
            if geoplaces.count != 0 {
                locationManager.stopUpdatingLocation()
            }
        }
        updateMapAnnotations()
        if let geoplace = selectedGeoplace {
            self.zoomToLocation(geoplace.latitude,
                withlongitude: geoplace.longitude,
                withViewLat: geoplace.viewLatitude,
                withViewLong: geoplace.viewLongitude,
                withAltitude: geoplace.viewAltitude,
                withViewHeading: geoplace.viewHeading)
            self.navigationController?.navigationBar.topItem?.title = geoplace.placename
        }
    }
    //MARK: Custom Functions
    func addAllAnnotations() {
        for place in geoplaces {
            if !doesAnnonationExist(place.latitude, longitude: place.longitude){
                addAnnotation(place.latitude, andlongitude: place.longitude, withTitle: place.placename, withSubtitle: "")
            }
        }
        
        if geoplaces.count > 0 {
            setRegion(true)
            changeNavbarTitleForMultiplePlaces()
        }
    }
    
    func addAnnotation(latitude: Double, andlongitude longitude: Double,
        withTitle title: String, withSubtitle subtitle: String) -> MKPointAnnotation{
            var annotation = MKPointAnnotation()
            annotation.title = title
            annotation.subtitle = subtitle
            annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude)
            mapView.addAnnotation(annotation)
            return annotation
    }
    func removeAllAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
    }
    func updateMapAnnotations() {
        removeAllAnnotations()
        addAllAnnotations()
    }
    func doesAnnonationExist(latitude: Double, longitude: Double) -> Bool{
        for annotation in mapView.annotations {
            if (annotation as MKPointAnnotation).coordinate.latitude == latitude
                && (annotation as MKPointAnnotation).coordinate.longitude == longitude {
                return true
            }else {
                return false
            }
            
        }
        return false

    }
    func zoomToLocation(latitude: Double, withlongitude longitude: Double,
        withViewLat viewLat: Double?, withViewLong viewLong: Double?, withAltitude altitude: Double?,
        withViewHeading viewHeading: Double?) {
        
            var cameraAltitude: Double = 300
            var viewLatFinal = latitude
            var viewLongFinal = longitude
            var viewHeadingFinal = 0.0
            if let alt = altitude {
                cameraAltitude = alt
            }
            if let viewLatitude = viewLat {
                viewLatFinal = viewLatitude
            }
            if let viewLongitude = viewLong {
                viewLongFinal = viewLongitude
            }
            if let viewHead = viewHeading {
                viewHeadingFinal = viewHead
            }
            
            let camera = MKMapCamera(
                lookingAtCenterCoordinate: CLLocationCoordinate2DMake(latitude, longitude),
                fromEyeCoordinate: CLLocationCoordinate2DMake(viewLatFinal, viewLongFinal),
                eyeAltitude: cameraAltitude)
            camera.heading = viewHeadingFinal
            mapView.setCamera(camera, animated: true)
    }
    func setPinColor() -> MKPinAnnotationColor {
        if let parentBookId = bookIdForPinColor {
            switch parentBookId {
            case 1 :
                return MKPinAnnotationColor.Red
            case 2:
                return MKPinAnnotationColor.Green
            case 4:
                return MKPinAnnotationColor.Purple
            default:
                return MKPinAnnotationColor.Red
            }
        }else {
            return MKPinAnnotationColor.Red
            
        }
    }
    
    func changeNavbarTitleForMultiplePlaces() -> String {
        var titlesDict =  [Int : String]()
        var titlesStringArray = [String]()
        for place in geoplaces {
            titlesDict[place.id] = place.placename
        }
        let places = ", ".join(Array(titlesDict.values) as [String])
        if places.lengthOfBytesUsingEncoding(NSUTF8StringEncoding) > 16 {
            
            return "\((places as NSString).substringWithRange(NSRange(location: 0, length: 16)))..."
        }
        return places
        
    }

    //MARK: Map View Delegate
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        let reuseId = "Pin"
        var view = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if view == nil {
            var pinView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pinView.canShowCallout = true
            pinView.animatesDrop = true
            pinView.pinColor = setPinColor()
            view = pinView
        }else {
            (view as MKPinAnnotationView).pinColor = setPinColor()
            view.annotation = annotation
        }
        
        return view
    }
    
    
    
    //MARK: Actions

    @IBAction func setRegion(sender: AnyObject) {
        if mapView.annotations.count > 0 {
            mapView.showAnnotations(mapView.annotations, animated: true)
            
            if mapView.annotations.count != 1 && mapView.annotations[0].subtitle != "Current Location" {
                // This method will show all the annotations on the map in one view
                self.navigationController?.navigationBar.topItem?.title = self.changeNavbarTitleForMultiplePlaces()
            }
        }
        
    }
    
    //MARK: CLLocation Delegate Methods
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        self.locationManager.stopUpdatingLocation()
        CLGeocoder().reverseGeocodeLocation(manager.location, completionHandler: {
            placemarks, error in
            if error == nil && placemarks.count > 0 {
                let pm = placemarks[0] as CLPlacemark
                let currentLocationAnnotation = self.addAnnotation(manager.location.coordinate.latitude, andlongitude: manager.location.coordinate.longitude, withTitle: "Current Location", withSubtitle: pm.name)
                self.mapView.selectAnnotation(currentLocationAnnotation, animated: true)
                self.navigationController?.navigationBar.topItem?.title = "\((pm.name as NSString).substringWithRange(NSRange(location: 0, length: 16)))..."
                self.zoomToLocation(manager.location.coordinate.latitude,
                    withlongitude: manager.location.coordinate.longitude,
                    withViewLat: nil,
                    withViewLong: nil,
                    withAltitude: 300,
                    withViewHeading: 0.0)

            }
        })
        
        
    }
    
    //MARK: Map Long Press Handler
    func mapLongPressed(gestureRecognizer : UIGestureRecognizer) {
        
 
        
        
//        var pa = MKPointAnnotation()
//        pa.coordinate = coordinate
//        pa.title = "Something"
//        mapView.addAnnotation(pa)
        }
    

    

  


}


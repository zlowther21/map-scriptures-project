//
//  FormTableViewController.swift
//  Map Scriptures Project
//
//  Created by Zachary Lowther on 12/10/14.
//  Copyright (c) 2014 Zachary Lowther. All rights reserved.
//

import UIKit

class FormTableViewController: UITableViewController, UITextFieldDelegate {
    var latText, longText, viewLatText, viewLongText, viewTiltText, viewRollText, viewAltText, viewHeadingText : String?
    @IBOutlet weak var latitude: UITextField!
    @IBOutlet weak var longitude: UITextField!
    @IBOutlet weak var viewLatitude: UITextField!
    @IBOutlet weak var viewLongitude: UITextField!
    @IBOutlet weak var viewTilt: UITextField!
    @IBOutlet weak var viewRoll: UITextField!
    @IBOutlet weak var viewAltitude: UITextField!
    @IBOutlet weak var viewHeading: UITextField!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        
        saveButton.enabled = false
        latitude.text = latText != nil ? latText : ""
        longitude.text = longText != nil ? longText : ""
        viewLatitude.text = viewLatText != nil ? viewLatText : ""
        viewLongitude.text = viewLongText != nil ? viewLongText : ""
        viewTilt.text = viewTiltText != nil ? viewTiltText : ""
        viewRoll.text = viewRollText != nil ? viewRollText : ""
        viewAltitude.text = viewAltText != nil ? viewAltText : ""
        viewHeading.text = viewHeadingText != nil ? viewHeadingText : ""
        
        //Pattern obtained from http://stackoverflow.com/questions/7010547/uitextfield-text-change-event
        latitude.addTarget(self, action: "textFieldDidChange:", forControlEvents: .EditingChanged)
        longitude.addTarget(self, action: "textFieldDidChange:", forControlEvents: .EditingChanged)
        
        if latitude.text != nil {
            textFieldDidChange(self)
        }else if longitude.text != nil {
            textFieldDidChange(self)
        }
        
        
        
        
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if let nextField = view.viewWithTag(textField.tag + 1){
            nextField.becomeFirstResponder()
        }
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        textField.selectAll(nil)
    }
    
    func textFieldDidChange(sender : AnyObject) {
        if !latitude.text.isEmpty && !longitude.text.isEmpty {
            saveButton.enabled = true
        }else {
            saveButton.enabled = false
        }
        
    }
}
